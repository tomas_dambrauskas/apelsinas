function changeMainPageForm() {
	var chartButton = document.getElementById('buttonForChart');
	var chartForm = document.getElementById('formForChart');
	if (chartButton.value == "Hide chart") {
		chartForm.style.display = "none";
		chartButton.value = "Show chart";
	} else {
		chartForm.style.display = "block";
		chartButton.value = "Hide chart";
	}
}