package lt.infobalt.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lt.infobalt.entities.Coupon;
import lt.infobalt.entities.Product;

public class CouponJPA {

	static final Logger log = LoggerFactory.getLogger(CouponJPA.class);
	EntityManagerFactory emf;

	public EntityManagerFactory getEmf() {
		return emf;
	}

	public void setEmf(EntityManagerFactory emf) {
		this.emf = emf;
	}

	public void save(Coupon coupon) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			if (!em.contains(coupon))
				coupon = em.merge(coupon);
			em.persist(coupon);
			em.getTransaction().commit();
		} finally {
			em.close();
		}
	}

	public void delete(Coupon coupon) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			coupon = em.merge(coupon);
			em.remove(coupon);
			em.getTransaction().commit();
		} finally {
			em.close();
		}
	}

	public List<Coupon> getAllCoupons() {
		EntityManager em = emf.createEntityManager();
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<Coupon> cq = cb.createQuery(Coupon.class);
			Root<Coupon> root = cq.from(Coupon.class);
			cq.select(root);
			TypedQuery<Coupon> q = em.createQuery(cq);
			return q.getResultList();
		} finally {
			em.close();
		}
	}
	
	public void addProduct(Product product) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			if (!em.contains(product)) {
				product = em.merge(product);
			}
			em.persist(product);
			em.getTransaction().commit();
		} finally {
			em.close();
		}
	}
	
	public void deleteProduct(Product product) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			em.remove(em.contains(product) ? product : em.merge(product));
			em.getTransaction().commit();
		} finally {
			em.close();
		}
	}

	public List<Product> getAllProductsForCoupon(Coupon coupon) {
		EntityManager em = emf.createEntityManager();
		try {
			coupon = em.merge(coupon);
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<Product> cq = cb.createQuery(Product.class);
			Root<Product> root = cq.from(Product.class);
			cq.select(root);
			cq.where(cb.equal(root.get("coupon"), coupon));
			TypedQuery<Product> q = em.createQuery(cq);
			return q.getResultList();
		} finally {
			em.close();
		}
	}
	public Double maxPriceOfProduct() {
		EntityManager em = emf.createEntityManager();
		try {
			TypedQuery<Double> tq = em.createQuery("SELECT max(i.productPrice) From Product i", Double.class);
			return tq.getSingleResult();
		} finally {
			em.close();
		}
	}
}
