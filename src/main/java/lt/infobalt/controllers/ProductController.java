package lt.infobalt.controllers;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import lt.infobalt.entities.Product;
import lt.infobalt.jpa.CouponJPA;
import lt.infobalt.models.CouponModel;
import lt.infobalt.models.ProductModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProductController implements Serializable {
	static final Logger log = LoggerFactory.getLogger(ProductController.class);

	private static final long serialVersionUID = 3592326513004966434L;
	private CouponModel couponModel;
	private ProductModel productModel;
	private CouponJPA couponJPA;
	public static final String NAV_VIEW_IN = "couponView";
	public static final String NAV_FORM_IT_EDIT = "productEditForm";
	public static final String NAV_FORM_IT_CREATE = "productCreateForm";

	public ProductController() {
	}

	public CouponModel getCouponModel() {
		return couponModel;
	}

	public void setCouponModel(CouponModel couponModel) {
		this.couponModel = couponModel;
	}

	public ProductModel getProductModel() {
		return productModel;
	}

	public void setProductModel(ProductModel productModel) {
		this.productModel = productModel;
	}

	public CouponJPA getCouponJPA() {
		return couponJPA;
	}

	public void setCouponJPA(CouponJPA couponJPA) {
		this.couponJPA = couponJPA;
	}

	public String create() {
		this.productModel.setCurrentProduct(new Product());
		return NAV_FORM_IT_CREATE;
	}

	public String update(Product product) {
		this.productModel.setCurrentProduct(product);
		return NAV_FORM_IT_EDIT;
	}

	public String delete(Product product) {
		this.couponJPA.deleteProduct(product);
		return NAV_VIEW_IN;
	}

	public String cancel() {
		this.productModel.setCurrentProduct(new Product());
		return NAV_VIEW_IN;
	}

	public String save() {
		this.productModel.getCurrentProduct().setCoupon(this.couponModel.getCurrentCoupon());
		this.couponJPA.addProduct(this.productModel.getCurrentProduct());
		return NAV_VIEW_IN;
	}

	public void priceColor() {
		RequestContext rc = RequestContext.getCurrentInstance();
		boolean isPriceTooHigh;
		ProductModel productModel = getProductModel();
		if (productModel == null) {
		    return;
		}
		Product currentProduct = productModel.getCurrentProduct();
		if (currentProduct == null) {
		    return;
		}
		if (currentProduct.getProductQuantity() == null || currentProduct.getProductPrice() == null) {
		    return;
		}
		Double sum = currentProduct.getProductQuantity() * currentProduct.getProductPrice();

		if (sum < 0) {
			rc.execute("document.getElementById('productForm:priceIndicator').setAttribute('class', 'redColor')");
			isPriceTooHigh = false;
		}
		if (sum > 0) {
			rc.execute("document.getElementById('productForm:priceIndicator').setAttribute('class', 'greenColor')");
			isPriceTooHigh = false;
		}
		if (sum > 3456) {
			rc.execute("document.getElementById('productForm:priceIndicator').setAttribute('class', 'yellowColor')");
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Total price is bigger than 3456", "Info"));
			isPriceTooHigh = false;
		}
	}

	public void updateMeterGauge() {
		RequestContext rc = RequestContext.getCurrentInstance();
		rc.execute("document.getElementById('productForm:price').setAttribute('class', 'yellowColor')");
		rc.update("productForm:metergauge");
	}
}
