package lt.infobalt.controllers;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import org.h2.util.MathUtils;

import lt.infobalt.entities.Coupon;
import lt.infobalt.entities.Product;
import lt.infobalt.jpa.CouponJPA;
import lt.infobalt.models.CouponModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CouponController implements Serializable {
	static final Logger log = LoggerFactory.getLogger(CouponController.class);


	private static final long serialVersionUID = -1314989185002853577L;
	private CouponModel couponModel;
	private CouponJPA couponJPA;
	public static final String NAV_LIST_IN = "main";
	public static final String NAV_FORM_IN = "couponForm";
	public static final String NAV_VIEW_IN = "couponView";
	public static final BigDecimal EUR = BigDecimal.valueOf(3.4528);

	public CouponController() {
	}

	public CouponJPA getCouponJPA() {
		return couponJPA;
	}

	public void setCouponJPA(CouponJPA couponJPA) {
		this.couponJPA = couponJPA;
	}

	public CouponModel getCouponModel() {
		return couponModel;
	}

	public void setCouponModel(CouponModel couponModel) {
		this.couponModel = couponModel;
	}

	public String save() {
		this.couponJPA.save(this.couponModel.getCurrentCoupon());
		return NAV_LIST_IN;
	}

	public String update(Coupon coupon) {
		this.couponModel.setCurrentCoupon(coupon);
		return NAV_FORM_IN;
	}

	public String delete(Coupon coupon) {
		this.couponJPA.delete(coupon);
		return NAV_LIST_IN;
	}

	public String create() {
		this.couponModel.setCurrentCoupon(new Coupon());
		return NAV_FORM_IN;
	}

	public String cancel() {
		this.couponModel.setCurrentCoupon(null);
		return NAV_LIST_IN;
	}

	public String view(Coupon coupon) {
		couponModel.setCurrentCoupon(coupon);
		return NAV_VIEW_IN;
	}

	public List<Coupon> getAllCoupons() {
		return this.couponJPA.getAllCoupons();
	}

	public List<Product> getAllProducts(Coupon coupon) {
		return this.couponJPA.getAllProductsForCoupon(coupon);
	}

	public BigDecimal getCouponSumLT(Coupon coupon) {
		List<Product> products = this.getAllProducts(coupon);
		Double couponSum = 0.0;
		BigDecimal roundedSum = null;
		if (products.isEmpty()) {
			return BigDecimal.valueOf(0.0);
		} else {
			for (Product x : products) {
				couponSum = couponSum + x.getProductPrice()*x.getProductQuantity();
				roundedSum = BigDecimal.valueOf(couponSum);
				roundedSum = roundedSum.setScale(2, RoundingMode.HALF_UP);
			}
			return roundedSum;
		}
	}

	public BigDecimal getCouponSumEU(Coupon coupon) {
		return getCouponSumLT(coupon).divide(EUR,2, RoundingMode.HALF_UP);
	}

	public boolean showNotificationBarForCoupons() {
		return this.getAllCoupons().size() < 2;
	}

	public boolean showNotificationBarForProducts(Coupon coupon) {
		return this.getAllProducts(coupon).size() < 1;
	}
}
