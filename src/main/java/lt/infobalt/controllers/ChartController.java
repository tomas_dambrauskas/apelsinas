package lt.infobalt.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;
import org.primefaces.model.chart.MeterGaugeChartModel;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Calendar;

import lt.infobalt.entities.Coupon;
import lt.infobalt.entities.Product;
import lt.infobalt.jpa.CouponJPA;
import lt.infobalt.models.ProductModel;
import lt.infobalt.models.CouponModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ChartController implements Serializable {
	static final Logger log = LoggerFactory.getLogger(ChartController.class);


	private static final long serialVersionUID = 8115155214999829815L;
	private MeterGaugeChartModel meterGaugeModel;
	private LineChartModel lineModel;
	private CouponJPA couponJPA;
	private ProductModel productModel;
	private CouponModel couponModel;

	public ChartController() {
	}
	
	public CouponModel getCouponModel() {
		return couponModel;
	}

	public void setCouponModel(CouponModel couponModel) {
		this.couponModel = couponModel;
	}

	public ProductModel getProductModel() {
		return productModel;
	}

	public void setProductModel(ProductModel productModel) {
		this.productModel = productModel;
	}

	public LineChartModel getLineModel() {
		return lineModel;
	}
	
	public void setLineModel(LineChartModel lineModel) {
		this.lineModel = lineModel;
	}

	public String generateBarcode() {
//		CouponModel couponModel = getCouponModel();
//		if (couponModel == null) {
//		    return "000000000000";
//		}
//		Coupon currentCoupon = couponModel.getCurrentCoupon();
//		if (currentCoupon == null) {
//		    return "000000000000";
//		}
//		String couponNumber = currentCoupon.getCouponNumber();
//		if (couponNumber.length() == 0) {
//		    return "000000000000";
//		}
		
		String couponNumber = couponModel.getCurrentCoupon().getCouponNumber();
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		Date today = Calendar.getInstance().getTime(); 
		String couponDate = df.format(today);
		return couponDate + couponNumber;
	}
	
	public CouponJPA getCouponJPA() {
		return couponJPA;
	}

	public void setCouponJPA(CouponJPA couponJPA) {
		this.couponJPA = couponJPA;
	}

	public MeterGaugeChartModel getMeterGaugeModel() {
		if (meterGaugeModel == null) {
			createMeterGaugeModels();
		}
		return meterGaugeModel;
	}
	
	public void setMeterGaugeModel(MeterGaugeChartModel meterGaugeModel) {
		this.meterGaugeModel = meterGaugeModel;
	}
	
	public void init() {
		createLineModels();
		createMeterGaugeModels();
	}
	
	private void createLineModels() {
		createLineModel();
	}
	
	private void createLineModel() {
		lineModel = initLineModel();
		lineModel.setTitle("Coupon chart");
		lineModel.setLegendPosition("e");
		Axis xAxis = lineModel.getAxis(AxisType.X);
		xAxis.setLabel("Prices");
		Axis yAxis = lineModel.getAxis(AxisType.Y);
		yAxis.setLabel("Quantity");
	}
	
	private LineChartModel initLineModel() {
		LineChartModel model = new LineChartModel();
		
		LineChartSeries series1 = new LineChartSeries();
		series1.setLabel("1-2 products");
		for (Coupon coupon : this.couponJPA.getAllCoupons()) {
			seriesForOneTwoProducts(series1, coupon);
		}
		
		LineChartSeries series2 = new LineChartSeries();
		series2.setLabel("More than 2 products");
		for (Coupon coupon : this.couponJPA.getAllCoupons()) {
			seriesForMoreThanTwoProducts(series2, coupon);
		}
		model.addSeries(series1);
		model.addSeries(series2);
		return model;
	}
	
	public void seriesForOneTwoProducts(LineChartSeries series1, Coupon coupon) {
		List<Product> products = this.couponJPA.getAllProductsForCoupon(coupon);
		Double couponPrice = 0.0;
		for (Product product : products) {
			if (product.getProductQuantity() == 2 || product.getProductQuantity() == 1 ) {
				couponPrice = couponPrice + product.getProductPrice();
			}
		}
		series1.set(coupon.getCouponNumber(), couponPrice);
	}
	
	public void seriesForMoreThanTwoProducts(LineChartSeries series2, Coupon coupon) {
		List<Product> products = this.couponJPA.getAllProductsForCoupon(coupon);
		Double couponPrice = 0.0;
		for (Product product : products) {
			if (product.getProductQuantity() > 2) {
				couponPrice = couponPrice + product.getProductPrice();
			}
		}
		series2.set(coupon.getCouponNumber(), couponPrice);
	}
	
	private MeterGaugeChartModel initMeterGaugeModel() {
		if (this.getCouponJPA().maxPriceOfProduct() == null) {
			Double maxPrice = 0.0;
		} else {
			this.getCouponJPA().maxPriceOfProduct();
		}
		List<Number> interval = new ArrayList<Number>() {
			{
				add(10);
				add(50);
				add(100);
				add(getCouponJPA().maxPriceOfProduct());
			}
		};
		Double currentPrice;
		if (this.productModel.getCurrentProduct().getProductPrice() == null) {
			currentPrice = 0.0;
		} else {
			currentPrice = this.productModel.getCurrentProduct().getProductPrice();
		}
		return new MeterGaugeChartModel(currentPrice, interval);
	}
	
	private void createMeterGaugeModels() {
		meterGaugeModel = initMeterGaugeModel();
		meterGaugeModel.setTitle("MeterGauge Chart");
		meterGaugeModel.setGaugeLabel("Price");
	}
}
