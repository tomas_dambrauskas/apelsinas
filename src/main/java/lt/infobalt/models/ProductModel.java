package lt.infobalt.models;

import java.io.Serializable;

import javax.validation.Valid;

import lt.infobalt.entities.Product;
import lt.infobalt.jpa.CouponJPA;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProductModel implements Serializable {
	
	static final Logger log = LoggerFactory.getLogger(ProductModel.class);

	private static final long serialVersionUID = 94603429755460270L;
	@Valid
	private Product currentProduct;

	public ProductModel() {

	}

	public void init() {
		this.currentProduct = new Product();
	}

	public Product getCurrentProduct() {
		return currentProduct;
	}

	public void setCurrentProduct(Product currentProduct) {
		this.currentProduct = currentProduct;
	}
}
