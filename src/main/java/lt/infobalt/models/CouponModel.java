package lt.infobalt.models;

import java.io.Serializable;

import javax.validation.Valid;

import lt.infobalt.entities.Coupon;
import lt.infobalt.jpa.CouponJPA;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CouponModel implements Serializable {
	static final Logger log = LoggerFactory.getLogger(CouponModel.class);

	private static final long serialVersionUID = 7014764155990212002L;
	@Valid
	private Coupon currentCoupon;

	public CouponModel() {
	}

	public void init() {
		this.currentCoupon = new Coupon();

	}

	public Coupon getCurrentCoupon() {
		return currentCoupon;
	}

	public void setCurrentCoupon(Coupon currentCoupon) {
		this.currentCoupon = currentCoupon;
	}
}
