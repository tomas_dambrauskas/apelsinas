package lt.infobalt.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Coupon
 *
 */
@Entity

public class Coupon {

	@Id
	@GeneratedValue()
	private Long id;
	private String couponNumber;
	@Temporal(TemporalType.DATE)
	private Date couponDate;
	private String couponBuyer;
	private String couponSeller;
	@OneToMany(orphanRemoval = true, mappedBy = "coupon", fetch = FetchType.EAGER)
	private List<Product> couponProducts;

	public Coupon() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCouponNumber() {
		return couponNumber;
	}

	public void setCouponNumber(String couponNumber) {
		this.couponNumber = couponNumber;
	}

	public Date getCouponDate() {
		return couponDate;
	}

	public void setCouponDate(Date couponDate) {
		this.couponDate = couponDate;
	}

	public String getCouponBuyer() {
		return couponBuyer;
	}

	public void setCouponBuyer(String couponBuyer) {
		this.couponBuyer = couponBuyer;
	}

	public String getCouponSeller() {
		return couponSeller;
	}

	public void setCouponSeller(String couponSeller) {
		this.couponSeller = couponSeller;
	}

	public List<Product> getCouponProducts() {
		return couponProducts;
	}

	public void setCouponProducts(List<Product> couponProducts) {
		this.couponProducts = couponProducts;
	}

}
