package lt.infobalt.entities;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Product
 *
 */
@Entity

public class Product {

	@Id
	@GeneratedValue
	private Long id;
	@ManyToOne
	private Coupon coupon;
	private String productName;
	private Integer productQuantity;
	private Double productPrice;
	private String productUnit;

	public Product() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public Coupon getCoupon() {
		return coupon;
	}

	public void setCoupon(Coupon coupon) {
		this.coupon = coupon;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getProductQuantity() {
		return productQuantity;
	}

	public void setProductQuantity(Integer productQuantity) {
		this.productQuantity = productQuantity;
	}

	public Double getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(Double productPrice) {
		this.productPrice = productPrice;
	}

	public String getProductUnit() {
		return productUnit;
	}

	public void setProductUnit(String productUnit) {
		this.productUnit = productUnit;
	}

	public BigDecimal sum() {
		BigDecimal roundedSum = BigDecimal.valueOf(productQuantity * productPrice);
		roundedSum = roundedSum.setScale(2, RoundingMode.HALF_UP);
		return roundedSum;
	}
}
